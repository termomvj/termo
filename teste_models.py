from django.test import TestCase
from polls.models import banco_palavras,informacoes

class BancoTestCase(TestCase):

    

    def test_verificar_se_base_de_dados_tem_30_resgistros_ou_mais(self):

        palavras = list(informacoes.palavra)
        self.assertTrue(len(palavras) >= 30, "o banco de dados precisa conter 30 resgistros  ou mais")

    def test_verifica_palavra(self):
        palavras = list(informacoes.palavra)
        self.assertIn("arroz", palavras)

    def test_tamanho_de_todas_as_palavras(self):
        palavras = list(informacoes.palavra)
        for i in palavras:
            self.assertTrue(len(i) == 5, "todas as palavras precisam ter cinco letras")
        
    def test_todas_as_palavras_sao_strings(self):
        palavras = list(informacoes.palavra)
        for i in palavras:
            self.assertTrue(isinstance(i,str), "todos os registro precisam ser strings")

    def test_palavras_nao_podem_ser_vazias(self):
        palavras = list(informacoes.palavra)
        for i in palavras:
            self.assertIsNotNone(i, "os resgistros nao podem ser nulos")