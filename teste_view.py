from django.test import TestCase
from django.urls import reverse
from polls.models import banco_palavras, informacoes
from polls.views import termo
class QuestionIndexViewTests(TestCase):


    def test_existe_word_list(self):
        resposta = self.client.get(reverse("word_list"))
        self.assertEqual(resposta.status_code, 200)
        self.assertTemplateUsed(resposta, "polls/word_list.html")


    def test_existe_lobby(self):
        resposta = self.client.get(reverse("lobby"))
        self.assertEqual(resposta.status_code, 200)
        self.assertTemplateUsed(resposta, "polls/Lobby.html")
 
    def test_resposta_correta(self):
        resposta = self.client.post(reverse("termo"),{"termo.resposta":["termo.palavra"], "vidas":6})
        self.assertTrue(resposta.context["acertou"])


    def test_existe_jogo(self):
        resposta = self.client.get(reverse("termo"))
        self.assertEqual(resposta.status_code, 200)
        self.assertTemplateUsed(resposta, "polls/Jogo.html")

    def test_verifica_sessao_limpa(self):
        self.assertNotIn('palavraTermo', self.client.session)

    def test_testando_se_vida_atribuida(self):
        resposta = self.client.get(reverse("termo"))
        vidas = resposta.context['vidas']
        self.assertEqual(resposta.status_code, 200)
        self.assertEqual(vidas, 6)

    def test_vida_descontada_em_erros(self):
        palavras = list(informacoes.palavra)
        palavras = {'vidas': 6}
        resposta = self.client.post(reverse("termo"), palavras)
        vidas = resposta.context['vidas']
        self.assertLess(5, vidas)

    