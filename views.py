from django.shortcuts import render
from .models import banco_palavras, informacoes
import random



def lobby(request):
    return render(request, "polls/Lobby.html")


def word_list(request):
    palavras = banco_palavras.objects.all()
    return render(request, "polls/word_list.html", {"palavras": palavras})

def termo(request):
    informacao = informacoes.objects.all()
    palavra = random.choice(informacoes.palavra)
    conjuntoDeLetras = list(palavra)
    palavraTermo = request.session.get("palavraTermo", [''] * 5)
    vidas = 6
    acertou = False
    perdeu = False

    
    if request.method == 'POST':
        resposta = request.POST.get('resposta', '').upper()
        palavra = request.POST.get('palavra', '').upper()
        vidas = int(request.POST.get('vidas', ))
        conjuntoDeLetras = list(palavra)

        if resposta == palavra:
            request.session.flush()
            acertou = True
        else:
            letras_corretas = []
            vidas -= 1
            if vidas == 0:
                request.session.flush()
                perdeu = True
                
            for contador,letras_certas in enumerate(list(resposta)):
                if letras_certas == conjuntoDeLetras[contador]:
                    letras_corretas.append(letras_certas)
                else:
                    letras_corretas.append("_")
            
            for i,acertos in enumerate(letras_corretas):
                if acertos != "_":
                    palavraTermo[i] = acertos
            if perdeu == False :
                request.session['palavraTermo'] = palavraTermo
    else:
        request.session.flush()


    return render(request, "polls/Jogo.html", {"informacao": informacao, 
                                                    "palavra":palavra, 
                                                    "conjuntoDeLetras": conjuntoDeLetras,
                                                    "vidas": vidas,
                                                    "palavraTermo": palavraTermo,
                                                    "acertou":acertou,
                                                    "perdeu":perdeu,
                                                    })

